package main

import (
	"log"

	_ "bitbucket.org/iminsoft/rbac/db"
	"bitbucket.org/iminsoft/rbac/handler"
	"github.com/micro/go-micro"
)

func main() {
	service := micro.NewService(
		micro.Name("im.srv.rbac"),
	)

	service.Init()
	server := service.Server()
	handler.Register(server)

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}

}
