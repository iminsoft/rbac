package handler

import (
	"bitbucket.org/iminsoft/rbac/db"
	"bitbucket.org/iminsoft/rbac/model"
	"github.com/micro/go-micro/server"
	"golang.org/x/net/context"
)

type Rbac struct {
	repo db.Repository
}

func (e *Rbac) Roles(ctx context.Context, req *model.RolesRequest, res *model.RolesResponse) error {
	r := e.repo.Find(req.Gid)

	res.Roles = r.Roles

	return nil
}

func (e *Rbac) InRole(ctx context.Context, req *model.InRoleRequest, res *model.InRoleResponse) error {

	r := e.repo.Find(req.Gid)
	res.InRole = r.InRole(req.Role)
	return nil
}

func (e *Rbac) Create(ctx context.Context, req *model.CreateRequest, res *model.CreateResponse) error {
	e.repo.CreateOrUpdate(req)
	return nil
}

func Register(s server.Server) {

	h := &Rbac{
		repo: db.NewRepository(),
	}

	s.Handle(s.NewHandler(h))

	go ListenRecords(h)
}
