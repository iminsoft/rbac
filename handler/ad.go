package handler

import (
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"strings"

	"bitbucket.org/iminsoft/rbac/model"
	"github.com/micro/go-micro/broker"
	"golang.org/x/net/context"
)

type (
	AdRecord map[string][]string
)

func init() {
	if err := broker.Init(); err != nil {
		log.Fatalf("Broker Init error: %v", err)
	}
	if err := broker.Connect(); err != nil {
		log.Fatalf("Broker Connect error: %v", err)
	}
}

func ListenRecords(h *Rbac) {

	_, err := broker.Subscribe("ad.record", func(p broker.Publication) error {
		r := AdRecord{}
		json.Unmarshal(p.Message().Body, &r)

		req := createRequest(r)
		res := model.CreateResponse{}

		h.Create(context.Background(), &req, &res)
		return nil
	})
	if err != nil {
		fmt.Println(err)
	}
}

func createRequest(r AdRecord) model.CreateRequest {
	c := model.CreateRequest{}
	if o, is := r["objectGUID"]; is {
		od, _ := base64.StdEncoding.DecodeString(o[0])
		c.Gid = hex.EncodeToString(od)
	}

	if g, is := r["memberOf"]; is {
		tmp := make(map[string]bool)
		for _, v := range g {

			cns := strings.SplitAfter(v, "CN=")
			if len(cns) == 0 {
				continue
			}

			gr := strings.Split(cns[1], ",")
			if len(gr) == 0 {
				continue
			}

			if _, ok := tmp[gr[0]]; ok {
				continue
			}

			tmp[gr[0]] = true

			c.Roles = append(c.Roles, strings.TrimSpace(gr[0]))
		}
	}

	return c
}
