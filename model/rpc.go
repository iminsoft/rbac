package model

type (
	RolesRequest struct {
		Gid string `json:"gid,omitempty"`
	}

	RolesResponse struct {
		Roles []string `json:"roles,omitempty"`
	}

	InRoleRequest struct {
		Gid  string
		Role string
	}

	InRoleResponse struct {
		InRole bool
	}

	CreateRequest struct {
		Gid   string
		Roles []string
	}

	CreateResponse struct {
	}
)
