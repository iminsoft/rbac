package model

type (
	Rbac struct {
		Gid   string   `json:"gid"`
		Roles []string `json:"roles"`
	}
)

func (r *Rbac) InRole(role string) bool {
	for _, v := range r.Roles {
		if v == role {
			return true
		}
	}

	return false
}
