package db

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/iminsoft/rbac/model"
	"github.com/boltdb/bolt"
)

var (
	rbacBucket = []byte("RBAC")
)

type (
	boltRepo struct {
		conn *bolt.DB
	}
)

func (d *boltRepo) Find(gid string) model.Rbac {
	o := &model.Rbac{}
	d.conn.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(rbacBucket)
		if b == nil {
			return fmt.Errorf("Bucket %q not found!", rbacBucket)
		}

		v := b.Get([]byte(gid))
		err := json.Unmarshal(v, o)
		if err != nil {
			return err
		}

		return nil
	})

	return *o
}

func (d *boltRepo) CreateOrUpdate(r *model.CreateRequest) {
	o := model.Rbac{Gid: r.Gid, Roles: r.Roles}
	d.conn.Batch(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists(rbacBucket)
		if err != nil {
			return err
		}

		ou, err := json.Marshal(o)
		if err != nil {
			return err
		}

		err = b.Put([]byte(r.Gid), ou)
		if err != nil {
			return err
		}

		return nil
	})
}

func NewRepository() Repository {
	return &boltRepo{
		conn: instance,
	}
}
