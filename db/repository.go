package db

import (
	"bitbucket.org/iminsoft/rbac/model"
)

type (
	Repository interface {
		CreateOrUpdate(r *model.CreateRequest)
		Find(gid string) model.Rbac
	}
)
