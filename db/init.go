package db

import (
	"log"

	"github.com/boltdb/bolt"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var (
	instance *bolt.DB
)

func init() {
	var err error
	instance, err = bolt.Open("rbac.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
}
